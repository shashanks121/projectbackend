app.config(['$routeProvider', function($routeProvider) {
            $routeProvider.when('/mapview',{
                    templateUrl: 'route/map.html',
                    controller: 'get_driver'
                }).
                when('/tableview',{
                    templateUrl: 'route/table.html',
                    controller: 'get_driver'
                }).
                otherwise({
                    redirectTo: '/'
                });
}]);